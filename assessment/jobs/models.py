from django.db import models
# View database via admin panel: http://localhost:8000/admin/

class Company(models.Model):
    name = models.TextField()
    image = models.URLField(null=True, blank=True)

    # Previously showed up as 'Companys' in admin. # https://stackoverflow.com/questions/31628918/django-spelling-mismatch-despite-not-being-written
    class Meta:
        verbose_name_plural = 'Companies' 

    def __str__(self):
        return self.name

# How to create database
# https://docs.djangoproject.com/en/3.2/intro/tutorial02/
# `python manage.py makemigrations jobs` to create migration files for my changes in jobs/migrations/models.py (this file)
# OPTIONAL: `python manage.py sqlmigrate jobs 0002` to check what SQL the migration would run.
# `python manage.py migrate` to apply/migrate changes to database.

# How to create fixtures (seed databases via .json file)
# https://docs.graphene-python.org/projects/django/en/latest/tutorial-plain/
# `python manage.py loaddata jobs` (jobs is name of app folder)

class Job(models.Model):
    title = models.TextField()
    company = models.TextField()
    location = models.TextField()
    salary_min = models.IntegerField()
    salary_max = models.IntegerField()
    apply_link = models.URLField(null=True, blank=True)
    lists = models.ManyToManyField('List') # List hasn't been created yet, so we use a string placeholder.

    def __str__(self):
        return self.title

class List(models.Model):
    name = models.TextField()
    description = models.TextField()
    image = models.URLField(null=True, blank=True)
    curator = models.TextField()
    jobs = models.ManyToManyField(Job) # https://docs.djangoproject.com/en/3.2/topics/db/models/


    def __str__(self):
        return self.name