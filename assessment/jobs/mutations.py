import graphene
from assessment.jobs.models import Company
from assessment.jobs.schema import CompanyType

# TODO: Create any necessary mutations you deem necessary

# This is an example mutation with inputs name (req'd), new_name, and new_image
class EditCompany(graphene.Mutation):
    class Arguments:
        name = graphene.String(required=True)
        new_name = graphene.String()
        new_image = graphene.String()

    company = graphene.Field(CompanyType)
    
    def mutate(root, info, name, new_name=None, new_image=None):
        try:
            c = Company.objects.get(name=name)
        except Company.DoesNotExist:
            return EditCompany(company=None)

        if new_name:
            c.name = new_name 
        if new_image:
            c.image = new_image

        c.save()
        return EditCompany(company=c)

class Mutation(graphene.ObjectType):
    edit_company = EditCompany.Field()
