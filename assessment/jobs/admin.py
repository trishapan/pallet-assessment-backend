# https://docs.graphene-python.org/projects/django/en/latest/tutorial-plain/
from django.contrib import admin

from .models import List, Job, Company

admin.site.register(List)
admin.site.register(Job)
admin.site.register(Company)