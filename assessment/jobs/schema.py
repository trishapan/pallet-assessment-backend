import graphene
from graphene_django import DjangoObjectType
from assessment.jobs.models import Company as CompanyModel
from assessment.jobs.models import List as ListModel
from assessment.jobs.models import Job as JobModel

class CompanyType(DjangoObjectType):
    class Meta:
        model = CompanyModel
    some_other_field = graphene.Int()
    # This is some random other field that shows how we can make new resolvers
    def resolve_some_other_field(parent, info):
        name = parent.name
        for letter in name:
            sum = sum + ord(letter)
        return sum

class ListType(DjangoObjectType):
    class Meta:
        model = ListModel
        fields = "__all__"

class JobType(DjangoObjectType):
    class Meta:
        model = JobModel

class Query(graphene.ObjectType):
    lists = graphene.List(ListType)
    filtered_jobs = graphene.List(
        JobType,
        lists=graphene.List(graphene.ID, required=True) 
    ) 

    # This is an example query that allows us to get the companies that contain the input 'name'
    companies = graphene.List(
        CompanyType,
        # Name is an input for the query
        name=graphene.String(required=True)
    )

    def resolve_companies(root, info, name):
        return CompanyModel.objects.filter(name__icontains=name)

    def resolve_lists(root, info): 
        return ListModel.objects.all()
        
    def resolve_filtered_jobs(root, info, lists):
        return JobModel.objects.filter(list__in=lists).distinct()

    # http://localhost:8000/graphql GraphiQL queries:
    # resolve_companies
    '''
    query Companies {
        companies(name: "Pallet") {
            name
        }
    }
    '''

    # resolve_lists
    '''
    { lists {
        name
        id
        }
    }
    '''

    # resolve_filtered_jobs
    '''
    query FilteredJobs { 
        filteredJobs(lists: [3,4]) {
                title
                company
                location
                salaryMin
                salaryMax
                applyLink 
            }
        }
    '''