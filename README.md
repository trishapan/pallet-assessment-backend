## Getting started

The following steps should help you get the server up and running:

Create a virtual environment, and activate it.

```
$ python3 -m venv venv
$ source venv/bin/activate
```

Upgrade pip and install the requirements.

```
$ pip install --upgrade pip
$ pip install -r requirements.txt
```

Migrate.

```
$ python manage.py migrate
```

Seed database in 'jobs' app.
```
$ python manage.py loaddata jobs
```

Run the server.

```
$ python manage.py runserver
```

Deactivate the virtual environment when you're done.

```
deactivate
```
